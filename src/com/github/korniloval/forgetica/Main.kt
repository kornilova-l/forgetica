package com.github.korniloval.forgetica

import org.telegram.telegrambots.ApiContextInitializer
import org.telegram.telegrambots.meta.TelegramBotsApi
import org.yaml.snakeyaml.Yaml
import java.io.FileInputStream
import java.lang.RuntimeException


fun main(args: Array<String>) {
    val yaml = Yaml()
    val obj = yaml.load<Map<String, Any>>(FileInputStream("knowledge.yaml"))
    val questions = obj.map { entry ->
        val value = entry.value
        when (value) {
            is String -> StringQuestion(entry.key, value)
            is Boolean -> BooleanQuestion(entry.key, value)
            else -> throw RuntimeException("Type is not known: " + value::class.java)
        }
    }.toList()
    ApiContextInitializer.init()
    val api = TelegramBotsApi()
    api.registerBot(ForgeticaBot(questions))
}
