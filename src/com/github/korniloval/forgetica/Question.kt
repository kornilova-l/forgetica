package com.github.korniloval.forgetica

interface Question {
    val question: String
    val answer: Any
    fun isCorrect(text: String): Boolean
}

class StringQuestion(override val question: String, override val answer: String) : Question {
    override fun isCorrect(text: String): Boolean = answer.toLowerCase() == text.toLowerCase()
}

class BooleanQuestion(override val question: String, override val answer: Boolean) : Question {
    override fun isCorrect(text: String): Boolean {
        val b = toBoolean(text) ?: return false
        return b == answer
    }

    private fun toBoolean(text: String): Boolean? =
            when (text.toLowerCase()) {
                "true", "yes", "да" -> true
                "false", "no", "нет" -> false
                else -> null
            }
}

