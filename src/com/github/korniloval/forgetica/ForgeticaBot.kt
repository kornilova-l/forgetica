package com.github.korniloval.forgetica

import org.telegram.telegrambots.bots.TelegramLongPollingBot
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Update
import java.io.File
import java.util.*

/**
 * @author Liudmila Kornilova
 **/
class ForgeticaBot(private val questions: List<Question>) : TelegramLongPollingBot() {
    private val lastQuestions = HashMap<Long, Question>()
    private val token = File("token.txt").readText().trim()
    private val myChatId = File("chatId.txt").readText().trim().toLongOrNull()
    private val random = Random()

    init {
        Timer().scheduleAtFixedRate(SendQuestionTask(), 0, 1000 * 60 * 60)
    }

    override fun getBotUsername(): String = "Forgetica"

    override fun getBotToken(): String = token

    override fun onUpdateReceived(update: Update) {
        val chatId = update.message.chatId
        lastQuestions[chatId]?.let { lastQuestion ->
            if (lastQuestion.isCorrect(update.message.text)) {
                execute(SendMessage(update.message.chatId, "Правильно"))
            } else {
                execute(SendMessage(update.message.chatId, lastQuestion.answer.toString()))
            }
            lastQuestions.remove(chatId)
        }
    }

    fun askIfNoQuestion(chatId: Long) {
        if (lastQuestions.containsKey(chatId)) return
        val question = randomQuestion()
        lastQuestions[chatId] = question
        execute(SendMessage(chatId, question.question))
    }

    private fun randomQuestion(): Question {
        return questions[random.nextInt(questions.size)]
    }

    inner class SendQuestionTask : TimerTask() {
        override fun run() {
            askIfNoQuestion(myChatId!!)
        }
    }
}
